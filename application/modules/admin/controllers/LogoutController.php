<?php


class Admin_LogoutController extends Admin_Controller_Action {
    
    public function init(){
        Zend_Auth::getInstance()->clearIdentity();
        $this->_helper->redirector->gotoRoute(array('action'=>'index',
            'controller'=>'login'),'default', true);
        //Vuelve a Login//
    }
    
}