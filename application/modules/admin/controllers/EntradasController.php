<?php


class Admin_EntradasController extends Admin_Controller_Action {

     public function init()
    {
        parent::init();
    }
 
    public function indexAction() {
        
    }
    
    public function borrarAction() {
        
    }
    
    public function nuevaAction() {
        //Coge el registro id_usuario//
        $Id_user = Zend_Auth::getInstance()->getIdentity()->id_usuario;
        $form = new Admin_Form_Entrada(); 
        //Coge el formulario Admin//
        //Si hay peticion y es valida,añade los registros en el formulario Admin//
        if($this->getRequest()->isPost()){
    
                $titulo = $this->_request->getPost('titulo');
                $entradilla =$this->_request->getPost('entradilla');
                $contenido =$this->_request->getPost('contenido');
                $slug = $this->_request->getPost('slug');
   
                //Obtenemos la fecha actual//
               $date = new Zend_Date();
               
                //Insertamos datos//
                $fila = array (
                    'titulo'   => $titulo,
                    'entradilla' => $entradilla,
                    'slug' => $slug,
                    'contenido' => $contenido,
                    'fecha_creacion' => $date->get('YYYY-MM-dd HH-mm-ss'),
                    //Introduciomos la fecha//
                    'fecha_edicion' => $date->get('YYYY-MM-dd HH-mm-ss'),
                    'activo' => 1,
                    'usuario_creacion_id' => $Id_user ,
                    'usuario_edicion_id' => $Id_user,                                   
                );
                
                $model_entrada = new Application_Model_Entrada();
                $model_entrada->insert($fila);
                
                $this->_helper->redirector->gotoRoute(array('action'=>'index',
                    'controller'=>'index'),'default', true);
        }     
        $this->view->form = $form;
  
    }
    
    
   
    
    
}
