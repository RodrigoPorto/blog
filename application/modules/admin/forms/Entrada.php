<?php

class Admin_Form_Entrada extends Zend_Form {

    //Formulario Admin con CkEditor//
    
    public function init() {

        $this->addElement('text', 'titulo');
        $this->getElement('titulo')
                ->addValidator('notEmpty', true, array('messages' => array(
                        Zend_Validate_NotEmpty::IS_EMPTY => 'Campo obligatorio'
            )))
                ->setLabel('Título')
                ->setRequired(true);

        $this->addElement('text', 'entradilla');
        $this->getElement('entradilla')
                ->addValidator('notEmpty', true, array('messages' => array(
                        Zend_Validate_NotEmpty::IS_EMPTY => 'Campo obligatorio'
            )))
                ->setLabel('Entradilla')
                ->setRequired(true);
        
        $this->addElement('text', 'slug');
        $this->getElement('slug')
                ->addValidator('notEmpty', true, array('messages' => array(
                        Zend_Validate_NotEmpty::IS_EMPTY => 'Campo obligatorio'
            )))
                ->setLabel('Slug')
                ->setRequired(true);

        $this->addElement('textarea', 'contenido');
        $this->getElement('contenido')
                 ->addValidator('notEmpty', true, array('messages' => array(
                        Zend_Validate_NotEmpty::IS_EMPTY => 'Campo obligatorio'
            )))
                ->setAttrib('class', 'ckeditor') //Le asinga una clase para Ckeditor//
                ->setLabel('Contenido')
                ->setRequired(true);

        $this->addElement('submit', 'submit');
        $this->getElement('submit')
                ->setLabel('ENVIAR');
    }

}
