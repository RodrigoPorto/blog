<?php

class LoginController extends My_Controller_Action {
    
    
   public function init(){
   
     parent::init();
    } 
    
    public function indexAction(){

        //Llama al form de Login//
        $form = new Application_Form_Login();
        //Verfifica la peticion//
        if($this->getRequest()->isPost()){
            
            if(!$form->isValid($this->getRequest()->getParams())){
                $form->populate($this->getRequest()->getParams());
            }else{
                //Toma los registros del formulario//
                $usuario = $form->getValue('usuario');
                $password = $form->getValue('password');
                
                $db = Zend_Db_Table::getDefaultAdapter();
                
                //Autenticacion//
                
                $authAdapter = new Zend_Auth_Adapter_DbTable($db);
                $authAdapter->setTableName('usuario')
                        ->setIdentityColumn('nombre')
                        ->setCredentialColumn('password')
                        ->setIdentity($usuario)
                        ->setCredential($password);
                
                $auth = Zend_Auth::getInstance();
                
                $resultado = $auth->authenticate($authAdapter);
          
                Zend_Debug::dump($resultado->isValid());
                
                //Si el usuario es valido,va a Admin//
                if ($resultado->isValid()){
                    //Almacena los datos obtenidos menos la contraseña//
                    $storage = $auth->getStorage();
                    $storage->write($authAdapter->getResultRowObject(
                                    null, 'password'
                    ));
                    //Redirige si es valido a nueva.phtml donde el controlador es EntradasController
                    // en admin//
                    $this->_helper->redirector->gotoRoute(array('action'=>'index',
                        'controller'=>'index'),'admin', true);
                    
                }else{
                   $this->view->error = "Credenciales incorrectas";
                }
              
                
            }
            
        }
        
        $this->view->form = $form;
      
    }
    
}